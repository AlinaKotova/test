class FormControl {

	constructor(type) {	
    }
	
	//Form showing if option is selected
	showForm(type) {
		$(".temp").hide();
		$("#"+type).show();
	}

	//Messege showing if option is not selected
	checkSwitcher () {
		if ($("#option option:selected").val() == "select") 
			alert("Please, select appropriate type of product!");
			else $("#form").submit();
	}
}

//Event handlers
$(document).ready(function () {   

	let control = new FormControl();

	$("#option").on("change",function () {
        control.showForm(this.value);	
	});
	
	$("#form").submit(function(e) {
		control.checkSwitcher();
		e.preventDefault();
	});  
});