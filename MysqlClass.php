<?php
    class MysqlClass {

        private $connection;
        private $query;
        private $arr;

        public function mysqlConnection() 
        {
            $this -> connection = new mysqli("localhost", "root", "root", "test");
            if ($this -> connection -> connect_errno) 
            {
                die("Connection failed: " . $this -> connection -> connect_error);
            }
        }

        public function mysqlQuery()
        {
            $this -> query = "SELECT * FROM product_list";
            $result = $this -> connection -> query($this -> query);
            $this -> arr = array();
            while ($row = $result -> fetch_assoc())
            {
                $this -> arr[] = $row;  
            }
            $result -> close();
            $this -> connection -> close();
        } 

        public function getArray() 
        {
            return $this->arr;
        }
    }
?>