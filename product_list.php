<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>   
    <link href="product_list.css" type="text/css" rel="stylesheet">
    <title>Product list</title> 
</head>

<body>
    <!-- Additional delete function -->
    <script type="text/javascript" src="delete.js"></script> 

    <!-- Database Class -->
    <?php  include("MysqlClass.php"); ?>

    <!-- Getting data from database -->
    <?php 
        $mysql = new MysqlClass;
        $mysql -> mysqlConnection();
        $mysql -> mysqlQuery();
        $array = $mysql -> getArray();
    ?>

    <!-- Page Header -->
    <div class="header"> 
        <h1>Product List</h1> 
        <div class="header_box">
            <select><option>Mass Delete Action</option></select> 
            <button type="button" value="click" onclick="deleteBox()"> Apply </button> 
        </div> 
    </div> 

    <!-- Product boxes -->
    <div class="products">  
        <!-- Setting Values -->
        <?php 
            $temp = 0;
            foreach($array AS $row) {
            $temp++;   
        ?>
            <div class="container" id="<?php echo $temp;?>">
                <input class="checkbox"  id="<?php echo $temp?>" type="checkbox" >
                <div>
                    <ul>
                        <input type="text" class="record" value = "<?php echo $row['product_sku'];?>">
                        <input type="text" class="record" value = "<?php echo $row['product_name'];?>">
                        <input type="text" class="record" value = "<?php echo $row['product_price'];?>">
                        <input type="text" class="record" value = "<?php echo $row['product_option']. " " .$row['option_value'];?>">     
                    </ul>          
                </div>
            </div>       
        <?php } ?> 
    </div> 

</body>
</html>