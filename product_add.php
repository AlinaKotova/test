<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href="product_add.css" rel="stylesheet">
        <title>Product add</title>
    </head>

    <body>

        <!-- Form change function -->
        <script type="text/javascript" src="change.js"></script> 
        
        <form id="form" action="<?=$_SERVER['PHP_SELF'];?>" method="post">

            <!-- Page Header -->
            <div class="header">  
                <h1>Product Add</h1> 
                <input id="button" type="submit" value="Save">
            </div> 

            <div class="wrapper">

                <!-- Product static form -->
                <p>
                    <label>Sku:
                        <input type="text" required placeholder="Enter a valid data">
                    </label> 
                </p>
                <p>
                    <label>Name: 
                        <input type="text" required placeholder="Enter a valid data">
                    </label>
                </p>
                <p>
                    <label>Price: 
                        <input type="text" required placeholder="Enter a valid data" pattern="\d+(\.\d{2})?"> <!-- validation for float number -->
                    </label>
                </p>           

                <!-- Type Switcher -->    
                <p>
                    <label>Type Switcher:
                        <select id="option" onchange="">
                            <option>select</option>
                            <option>Disk</option>
                            <option>Book</option>
                            <option>Furniture</option>
                        </select> 
                    </label> 
                </p>

                <!-- Product dynamic forms -->  
                <div class="temp" id="Disk" style="display: none">
                    <p>Size:
                        <input type="text" required placeholder="Enter a valid data" pattern="^[ 0-9]+$"> <!-- validation for integer number only -->
                    </p>
                    <p>Please provide information in Mb!
                    </p>
                </div>

                <div class="temp" id="Book" style="display: none">
                    <p>Weight:
                        <input type="text" required placeholder="Enter a valid data" pattern="^[ 0-9]+$"> <!-- validation for integer number only -->
                    </p>
                    <p>Please provide information in Kg!
                    </p>
                </div>

                <div class="temp" id="Furniture" style="display: none">
                    <p>Height:
                        <input type="text" required placeholder="Enter a valid data" pattern="\d+(\.\d{0,2})?"> <!-- validation for float only -->
                    </p>
                    <p>Width:
                        <input type="text" required placeholder="Enter a valid data" pattern="\d+(\.\d{0,2})?"> <!-- validation for float only -->
                    </p>
                    <p>Length:
                        <input type="text" required placeholder="Enter a valid data" pattern="\d+(\.\d{0,2})?"> <!-- validation for float only -->
                    </p>
                    <p>Please provide dimensions in HxWxL format!
                    </p>
                </div>

            </div>
        </form>
    </body>
</html>